extends Node

var dosiervojo : String = "usr://agordoj.cfg"

func _ready():
	malfermi_agordojn()

func malfermi_agordojn():
	var agord = ConfigFile.new()
	var eraro = agord.load(dosiervojo)
	if eraro:
		var nova_dosiero = ConfigFile.new()
		nova_dosiero.save(dosiervojo)
		konservi("agordoj", "muziko", false)
		konservi("agordoj", "sonoj", false)
		return
	
	AudioServer.set_bus_mute(1, agord.get_value("agordoj", "muziko", true))
	AudioServer.set_bus_mute(2, agord.get_value("agordoj", "sonoj", true))
		
func konservi(temo, afero, agordo):
	var agord = ConfigFile.new()
	agord.load(dosiervojo)
	agord.set_value(temo, afero, agordo)
	var eraro = agord.save(dosiervojo)
	print(eraro)

func _on_Muziko_toggled(button_pressed):
	if button_pressed:
		konservi("agordoj", "muziko", false)
	else:
		konservi("agordoj", "muziko", true)


func _on_Sonefektoj_toggled(button_pressed):
	if button_pressed:
		konservi("agordoj", "sonoj", false)
	else:
		konservi("agordoj", "sonoj", true)