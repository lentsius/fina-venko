extends Camera2D

onready var esperantisto : KinematicBody2D = $'../Esperantisto'

var tremobligo = 5
var komencite = false
var tremas = false

var height : float = 360

var distanco : float = 0

func _physics_process(delta):
	if komencite:
		position.x = esperantisto.position.x + distanco
	else:
		position.x += 3
	if tremas:
		tremi()
		
	position.y = height

func _on_Area2D_body_entered(body):
	if body.name.begins_with("ter"):
		body.get_parent().queue_free()
	body.queue_free()

func tremi():
	offset = Vector2(rand_range(-tremobligo, tremobligo), rand_range(-tremobligo, tremobligo))

	
func _komencite():
	komencite = true
	$Tween.interpolate_property(self, 'distanco', 0, 500, 2, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()

func _on_Esperantisto_tremi(kiom, tempo = 2.0):
	komenc_tremi(kiom, tempo)
	
func komenc_tremi(obligo : float = 5.0, tempo : float = 2.0):
	tremas = true
	tremobligo = obligo
	$Tween.interpolate_property(self, 'tremobligo', tremobligo, 0, tempo, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()
	yield($Tween, 'tween_completed')
	offset_h = 0
	offset_v = 0
	tremas = false
	

func _on_Esperantisto_sendi_staton(numero):
	if numero == 1:
		$Tween.interpolate_property(self, 'height', 360.712, 0, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	else:
		$Tween.interpolate_property(self, 'height', 0, 360.712,1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()