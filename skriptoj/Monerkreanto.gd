extends Position2D

onready var monero : PackedScene = preload("res://asetoj/stelo/stelo.tscn")
var loko : int = 120

signal kreite(kion)

func krei() -> void:
	var nova = monero.instance()
#	nova.scale = Vector2(0.75,0.75)
	nova.position = global_position
	nova.position.y = 0
	nova.position.y += rand_range(-loko, loko)
	emit_signal("kreite", nova)
	$Tempilo.start()

func _on_Tempilo_timeout():
	krei()
	
func sxalti() -> void:
	$Tempilo.wait_time = 0.1
	$Tempilo.start()

func malsxalti() -> void:
	$Tempilo.stop()