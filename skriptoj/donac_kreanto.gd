extends Position2D

### PRENEBLAJXOJ

onready var stelo : PackedScene = preload('res://asetoj/stelo/stelo.tscn')

### PLIRAPIDIGLO

onready var plirapidigilo : PackedScene = preload('res://asetoj/plirapidigilo/plirapidigilo.tscn')

### BARILOJ / OBSTAKLOJ

onready var obstaklo1 : PackedScene = preload('res://asetoj/sxtonoj/obstaklo1.tscn')
onready var obstaklo2 : PackedScene = preload('res://asetoj/sxtonoj/obstaklo2.tscn')
onready var obstaklo3 : PackedScene = preload('res://asetoj/sxtonoj/obstaklo3.tscn')
onready var barilo1 : PackedScene = preload('res://asetoj/sxtonoj/barilo01.tscn')
onready var barilo2 : PackedScene = preload('res://asetoj/sxtonoj/barilo02.tscn')
onready var barilo3 : PackedScene = preload('res://asetoj/sxtonoj/barilo03.tscn')

### VORT SCENOJ

#onready var HMD : PackedScene = preload('res://asetoj/monero/vortoj/HMD.tscn')
#onready var MIDZI : PackedScene = preload('res://asetoj/monero/vortoj/MIDZI.tscn')

### FONAJX OBJEKTOJ

onready var fonajxo1 : PackedScene = preload('res://asetoj/fonajxoj/Fonajxo01.tscn')
onready var fonajxo2 : PackedScene = preload('res://asetoj/fonajxoj/Fonajxo02.tscn')
onready var fonajxo3 : PackedScene = preload('res://asetoj/fonajxoj/Fonajxo03.tscn')

var obstakloj : Array = []
var vortoj : Array = []
var fonajxoj : Array = []

#var konstru_pravoj : Array = [false, false, false]
var konstrueblas : bool = true
var konstru_pozicio : Vector2

var automate = 0

signal kreite(kio)
#signal kreite_obstaklon(obstaklo)

func _ready():
	obstakloj.append(obstaklo1)
	obstakloj.append(obstaklo2)
	obstakloj.append(obstaklo3)
	obstakloj.append(barilo1)
	obstakloj.append(barilo2)
	obstakloj.append(barilo3)		
	position.x = get_parent().position.x + 900
#	vortoj.append(HMD)
	fonajxoj.append(fonajxo1)
	fonajxoj.append(fonajxo2)
	fonajxoj.append(fonajxo3)
	
func _process(delta):
	pass
#	position.x = get_parent().position.x + 900
	if Input.is_action_just_pressed("ui_accept"):
		konstrui_tere()
		konstrui_fonajxon()
		
				
func konstrui_tere() -> void:
		randomize()
		kontroli()
		if konstrueblas:
			var nova_objekto
			if automate == 0:
					
				var sxanco = rand_range(0,2)
				if sxanco < 0.5:
					nova_objekto = krei_obstaklon()
#				elif sxanco >= 0.5 and sxanco <= 1:
#					nova_objekto = krei_vorton()
				elif sxanco > 1 and sxanco <= 1.8:
					nova_objekto = krei_stelon()
	#			elif sxanco > 1.8:
				else:
					nova_objekto = krei_plirapidigilon()
				
			else:
				match automate:
					1:
						nova_objekto = krei_stelon()
					2:
						nova_objekto = krei_obstaklon()
					3:
						nova_objekto = krei_plirapidigilon()


				
			if nova_objekto != null:
				emit_signal("kreite", nova_objekto)
			else:
				print("konstruis nenion, agordis tempilon")
				agordi_tempon(0.5, 1.0)
		

func kontroli():
	if !$radioj/mez_radio.is_colliding():
		konstrueblas = false
	else:
		konstrueblas = true
		if $radioj/mez_radio.get_collider().name == "ter_fino" or $radioj/mez_radio.get_collider().name == "ter_komenco":
			konstrueblas = false
	
	konstru_pozicio = $radioj/mez_radio.get_collision_point()
	$tempilo.start()

func agordi_tempon(mini, maksi) -> void:
	$tempilo.wait_time = rand_range(mini,maksi)
	$tempilo.start()

### KREADO ###

func krei_stelon():
	var nova_stelo = stelo.instance()
	var alteco : float
	var hazardo = randf()

	#hazarde sxangxi la stelpozicion
	if hazardo > 0.5:
		alteco = -180
	else:
		alteco = -30
	
	var nova_pozicio = konstru_pozicio
	nova_pozicio.y += alteco
	nova_stelo.position = nova_pozicio
	
	agordi_tempon(0.2,0.5)
	
	if hazardo > 0.5:
		automate = 1
	else:
		automate = 0
	
	return nova_stelo

func krei_vorton():
	var nova_vorto = vortoj[randi() % vortoj.size()].instance()
	
	var novpozicio = konstru_pozicio
	nova_vorto.position = novpozicio
	
	$tempilo.wait_time = 4
	
	agordi_tempon(3, 5)
	
	
	return nova_vorto

func krei_plirapidigilon():
	var nova = plirapidigilo.instance()
	var alteco : float
	alteco = -180.0
	
	var nova_pozicio = konstru_pozicio
	nova_pozicio.y += alteco
	nova.position = nova_pozicio
	
	agordi_tempon(0.2,2.5)
	
	if randf() > 0.5:
		automate = 3
	else:
		automate = 0	
	
	return nova

func krei_obstaklon():
	var nova_obstaklo = obstakloj[randi() % obstakloj.size()].instance()
	nova_obstaklo.position = konstru_pozicio

	agordi_tempon(0.2,2.5)

	if randf() > 0.5:
		automate = 2
	else:
		automate = 0	

	return nova_obstaklo

func konstrui_fonajxon() -> void:
	randomize()
	kontroli()
	if !konstrueblas:
		return
	var fonajxo = fonajxoj[randi() % fonajxoj.size()].instance()
	
	var novpozicio = konstru_pozicio
	fonajxo.position = novpozicio
		
	$FonTempilo.wait_time = rand_range(1, 5)
	$FonTempilo.start()
	
	emit_signal("kreite", fonajxo)
	
### FINO DE KREADO ###

### SIGNALOJ SUBE ###

func _on_Esperantisto_pozicio(pozicio):
	self.position.x = pozicio.x + 1375

func _on_uzant_interfaco_komencite():
	agordi_tempon(4.0,6.1)
	$FonTempilo.start()

func _on_Esperantisto_sendi_staton(numero):
	pass

func _on_tempilo_timeout():
	print("provas konstrui ion")	
	konstrui_tere()

func _on_FonTempilo_timeout():
	print("provas konstrui fonajxon")
	konstrui_fonajxon()
