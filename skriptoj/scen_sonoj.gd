extends Node

func ludi_sonon(nomo):
	get_node(nomo).play()

func _on_Esperantisto_sxton_koliziis():
	ludi_sonon("kolizsono")

func _on_Esperantisto_donaco_kolektita():
	ludi_sonon("pren_sono")

func _on_Esperantisto_ekkurinte():
	ludi_sonon("fon_muziko")

func _on_uzant_interfaco_komencite():
	$piano_key_imact.play()
	$Tween.interpolate_property($birda_muziko, 'volume_db', -5, -50, 0.45, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()
	yield(get_tree().create_timer(0.30), 'timeout')
	$birda_muziko.playing = false
	ludi_sonon('falo')