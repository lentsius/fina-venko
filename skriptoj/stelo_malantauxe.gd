extends Sprite

export(Color, RGBA) var aktiva_koloro
export(Color, RGBA) var originala_koloro
var nova_grandeco = Vector2(0.6,0.6)

func _ready():
	modulate = originala_koloro
	scale = nova_grandeco

func konfirmi():
	$Tween.interpolate_property(self, 'modulate', aktiva_koloro, originala_koloro, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)
	$Tween.interpolate_property(self, 'scale', Vector2(1.5,1.5), nova_grandeco, 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)	
	$Tween.start()

func _on_Esperantisto_donaco_kolektita():
	konfirmi()
