extends CanvasLayer

onready var animacioj = $animacia_ludilo

#var altigx_kvanto = 5

var komencigxis : bool = false
#var poentoj : int
var steloj : int
var origina_ludat_pozicio : int

signal komencite

func _ready():
	set_process_unhandled_input(false)
	$kabeis.visible = false
	$komenco.visible = true
	$menubutono.visible = false
	$amfarite.visible = false

func _on_menubutono_toggled(button_pressed):
	print("klakite")
	if button_pressed:
		animacioj.play("menuo")
		get_tree().paused = true
	else:
		animacioj.play_backwards("menuo")
		$amfarite.visible = false
		$pri_la_ludo.visible = false
		get_tree().paused = false

func _on_Restartigxi_pressed():
	get_tree().paused = false
	var eraro = get_tree().reload_current_scene()
	print(eraro)
	
func _input(event):
	if event is InputEventScreenTouch:
		if komencigxis == false:
			komencigxis = true
			emit_signal("komencite")
			$menubutono.visible = true
			$komenco/komenc_ekrana_animacio.play("klak")
			set_process_input(false)

func _unhandled_input(event):
	if event is InputEventScreenTouch:
		kasxi_gvidon()
		set_process_unhandled_input(false)

func venki():
	pass

func malvenki(pauzi : bool) -> void:
	if pauzi:
		get_tree().paused = true
		$wah.play()
	$kabeis.visible = true
	$animacia_ludilo.play("kabei")

func _on_Esperantisto_donaco_kolektita() -> void:
	$VBoxContainer/poentoj/distanco2.text = str(steloj + 1)
	$tvino.interpolate_property($VBoxContainer/poentoj, 'modulate', Color(1,1,1,1), Color("7fffffff"), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$tvino.interpolate_property($VBoxContainer/poentoj, 'rect_scale', Vector2(3,3), Vector2(1,1), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)	
	$tvino.start()
	altigi_stelojn(1)
	

#kio okazos se la esperantisto kolizias kun la sxtonon
func _on_Esperantisto_sxton_koliziis() -> void:
	steloj = steloj - (steloj / 10) - 2
	if steloj < 0:
		malvenki(true)
		return
	
	#se ni havis suficxe a steloj - > ni daurigas	
	$tvino.interpolate_property($VBoxContainer/poentoj, 'modulate', Color(1, 0.286865, 0.007812), Color(1,1,1,1), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$tvino.start()
	$VBoxContainer/poentoj/distanco2.text = str(steloj)
	
func _on_esperanto_pressed() -> void:
	OS.shell_open("http://esperanto.net")
	
func _on_zemanhof_pressed() -> void:
	OS.shell_open("http://zamenhof.info")
	
func _on_pri_la_ludo_toggled(button_pressed) -> void:
	if button_pressed:
		$amfarite.visible = false
		$pri_la_ludo.visible = true
		$menuo/amo.pressed = false
	else:
		$pri_la_ludo.visible = false
	
func _on_Esperantisto_kabei() -> void:
	malvenki(true)
	
func _on_denove_pressed() -> void:
	get_tree().paused = false
	get_tree().reload_current_scene()
	
	
func _on_Sonefektoj_toggled(button_pressed) -> void:
	if button_pressed:
		AudioServer.set_bus_mute(2,0)
	else:
		AudioServer.set_bus_mute(2,1)

func _on_Muziko_toggled(button_pressed) -> void:
	if button_pressed:
		AudioServer.set_bus_mute(1,0)
	else:
		AudioServer.set_bus_mute(1,1)

func montri_gvidon() -> void:
	$gvid_mano.modulate = Color(1,1,1,0)
	$gvid_mano.visible = true
	$tvino.interpolate_property($gvid_mano, 'modulate', Color(1,1,1,0), Color(1,1,1,1), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$tvino.start()
	set_process_unhandled_input(true)
	
func kasxi_gvidon() -> void:
	$tvino.interpolate_property($gvid_mano, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 4, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$tvino.start()
	yield($tvino, 'tween_completed')
	$gvid_mano.visible = false
	
func _on_exit_button_pressed() -> void:
	get_tree().quit()
	
func _on_amo_toggled(button_pressed):
	if button_pressed:
		$amfarite.visible = false
	else:
		$pri_la_ludo.visible = false
		$menuo/menuo_horizontala/menuo_vertikala/pri_la_ludo.pressed = false
		$amfarite.visible = true

func _on_Esperantisto_pozicio(pozicio):
	$VBoxContainer/distanco.text = str(int(floor(pozicio.x - origina_ludat_pozicio) / 300)) + " m"
	
func _on_Esperantisto_unua_pozicio(pozicio):
	origina_ludat_pozicio = pozicio.x
	
func altigi_stelojn(kiom) -> void:
	steloj += 1

func _on_eci_pressed():
	OS.shell_open("http://ikso.net")
