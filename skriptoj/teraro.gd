extends Node2D

onready var terpeco = preload('res://asetoj/tero/ter_peco.tscn')

var konstrui_loko : Vector2

func _ready():
	randomize()
	konstrui_loko = $ter_peco.preni_finon()
	for i in range(0,20):
		konstrui_teron()
	
func konstrui_teron():
	randomize()
	
	var novtero = terpeco.instance()
	
	novtero.position = konstrui_loko
	
	novtero._ordoni_longecon()
	novtero.position.y = rand_range(konstrui_loko.y + 20, konstrui_loko.y -120)
	add_child(novtero)
	konstrui_loko.x = novtero.preni_finon().x + rand_range(0, 450)

func _on_ofteco_timeout():
	konstrui_teron()