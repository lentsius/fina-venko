extends KinematicBody2D

var direkto : Vector2 = Vector2(1,0)
var movigxado : Vector2

var ekrankareso : Vector2
var flugalteco : Vector2
var glatflugo : Vector2
#	jesauxnoj
var salteblas : bool = false
var reveneblas : bool = false
var komencite : bool = false
var tremis : bool = false
var blokita : bool = true
var revensonis : bool = false
var duasalto : bool = false
var povas_dusalti : bool = true

var jamsaltis : int = 0

var gravito = 9.3 * 3
var fin_gravito : float

var rapideco : float = 0
var rapid_obligo : float = 1.0
var salt_potenco : float = 900

#	statoj:
#	0 = kuras
#	1 = flugas
#
var stato : int = 0

signal donaco_kolektita
signal sxton_koliziis
signal kabei
signal tremi(kiom, tempo)
signal ekkurinte
signal pozicio(pozicio)
signal unua_pozicio(pozicio)
signal sendi_staton(numero)

#	nomoj de naimacioj estas
#	kuri
#	salti
#	fali
#	flugi
var nuna_animacio : String = "kuri"

func _ready():
	pass

func _process(delta):
	match stato:
		0:
			kuri_procezo(delta)
		1:
			pass
	
	if $radio.is_colliding():
		$ombro.position.y = to_local($radio.get_collision_point()).y
	else:
		$ombro.position.y = 10000
	
func _physics_process(delta):
	
	match stato:
		0:
			kuri(delta)
		1:
			flugi(delta)
			
	movigxado.x = direkto.x * rapideco * rapid_obligo * delta
	movigxado = move_and_slide(movigxado, Vector2(0,-1))

func flugi(delta):
	flugalteco.y += ekrankareso.y * 3
	glatflugo = glatflugo.linear_interpolate(flugalteco, 20 * delta)
	self.position.y = glatflugo.y
	
	ekrankareso = Vector2()

func kuri_procezo(delta):
	if position.y > 950:
		kabeas()
	if komencite && !blokita:
		$AnimationPlayer.playback_speed = (rapideco / 25000 ) * 3.35
#		rapideco += 1.5
		emit_signal("pozicio", position)
				
	if !is_on_floor() and movigxado.y < 0:
		sxangxi_animacion("salti")
		revensonis = false		
		
	elif !is_on_floor() and movigxado.y > 0:
		sxangxi_animacion("fali")
				
	if is_on_floor():
		jamsaltis = 0
		if !revensonis:
#			eble poste ni povas tremigi kameraon kiam la esperantisto surterigxas
#			emit_signal("tremi", 5)
			$Node/step01.play()
			$Node/step02.play()			
			revensonis = true
		if !blokita:
			sxangxi_animacion("kuri")
		if !tremis:
			sendi_tremon(50, 2)
			tremis = true
			surterigxi()

func kuri(delta) -> void:
	
	if !is_on_floor() && komencite:
		movigxado.y += gravito

	if salteblas == true and is_on_floor() and !blokita:
		salti()
	
	if !is_on_floor() and duasalto and povas_dusalti and jamsaltis <= 1:
		salti(true)
		povas_dusalti = false
		duasalto = false

	if Input.is_action_just_pressed("ui_accept"):
		emit_signal("donaco_kolektita")
	

func salti(dua_salto : bool = false):
	$Node/salti2.play()
	
#	if movigxado.y > 0:
	movigxado.y = 0
	if dua_salto:
		movigxado.y -= salt_potenco / 1.5
	else:
		movigxado.y -= salt_potenco
	salteblas = false
	jamsaltis += 1

func _unhandled_input(event):
	match stato:
		0:
			if event is InputEventScreenTouch:
				if event.pressed and is_on_floor():
					salteblas = true
				elif !event.pressed and !is_on_floor():
					povas_dusalti = true
				if event.pressed and !is_on_floor() and povas_dusalti:
					duasalto = true
					
		1:
			if event is InputEventScreenDrag:
				ekrankareso = event.relative

func sxangxi_animacion(nomo : String):
	if nuna_animacio != nomo:
		nuna_animacio = nomo
		$AnimationPlayer.play(nomo)

func kabeas():
#	get_tree().reload_current_scene()
	komencite = false
	$Tween.interpolate_property(self, 'rapideco', rapideco, 0, 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tween.start()
	emit_signal("kabei")	

###	sxangxado de la statojn
func _on_kolektejo_body_entered(body):
#	print("kolizio okazis")
	if body.is_in_group("donacoj"):
		body.la_fino()
		emit_signal("donaco_kolektita")

	elif body.is_in_group("plirapidigilo"):
		body.la_fino()
		$Tempilo.wait_time = 5
		$Tempilo.start()
		
		if stato != 1:
			#obligi rapidecon
			var nova_obligo : float = 3.0
			$Tween.interpolate_property(self, 'rapid_obligo', 1.0, 0.2, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
			$Tween.interpolate_property(self, 'rapid_obligo', 1.0, nova_obligo, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 1)
			$Tween.start()
			$Node/salti.play()
			flugalteco.y = 0
			glatflugo = position
			$AnimationPlayer.play("flugi")
			stato = 1
			emit_signal('sendi_staton', stato)
			yield(get_tree().create_timer(1), 'timeout')
			$FlugKapucxo.scale = Vector2(0.01,0.01)
			$FlugKapucxo.visible = true
			$Tween.interpolate_property($FlugKapucxo, 'scale', Vector2(0.01,0.01), Vector2(0.272,0.272), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
			$Tween.start()
			
			$Node/eksplodo.play()
			$FlugVento.visible = true
			$FlugVento.modulate = Color(1,1,1,1)
			$Monerkreanto.sxalti()
			
			sendi_tremon(20, 7)

	elif body.is_in_group("obstakloj"):
		emit_signal("tremi", 10)
		emit_signal("sxton_koliziis")		
		$Tween.interpolate_property(self, 'modulate', Color(1,0,0,1), Color(1,1,1,1), 0.5, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)
		$Tween.start()
		
		#	prifajfi sxtonetojn dum duonsekundo kaj restarti rapidecon
		self.set_collision_layer_bit(3, false)
		self.set_collision_mask_bit(3, false)
		rapideco = 25000
		yield(get_tree().create_timer(0.5), 'timeout')
		self.set_collision_layer_bit(3, true)
		self.set_collision_mask_bit(3, true)

### la flugtempilo!
func _on_Tempilo_timeout():
	$Tempilo.stop()
	if stato != 0: 
		$Tween.interpolate_property(self, 'rapid_obligo', 6.0, 1.0, 1.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
		$Tween.interpolate_property($FlugKapucxo, 'scale', Vector2(0.272,0.272), Vector2(0.01,0.01), 0.75, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
		$Tween.interpolate_property($FlugVento, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 1.5, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)		
		$Tween.start()
		$AnimationPlayer.play("fali")
		movigxado.y = 0
		stato = 0
		$Monerkreanto.malsxalti()
		emit_signal('sendi_staton', stato)
		
		yield($Tween, "tween_completed")
		$FlugKapucxo.visible = false
		$FlugVento.visible = false

func _on_uzant_interfaco_komencite():
	komencite = true
	rapideco = 25000

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "genui":
		blokita = false
		rapideco = 25000
		emit_signal("ekkurinte")
		emit_signal("unua_pozicio", position)
	
	$AnimationPlayer.play(nuna_animacio)

func sendi_tremon(kiom : float = 5.0, tempo : float = 2.0):
	emit_signal('tremi', kiom, tempo)
	
func surterigxi():
	$AnimationPlayer.play('genui', 1, 1)
	rapideco = 0
	
func ludi_pasx_sonon_1():
	$Node/step01.play()
	
func ludi_pasx_sonon_2():
	$Node/step02.play()	
	
func pasxe_tremigi_kameraon() -> void:
	emit_signal("tremi", 1)

func _on_salt_tempilo_timeout():
	povas_dusalti = true
