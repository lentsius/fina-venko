extends Node2D

#	min = 3 max = 20
#export(float) var longeco : float = 10
var truo = 250
var magia_numero = 132.5

func _ready():
	_ordoni_longecon()

func _ordoni_longecon():
	randomize()
	var nov_longeco = rand_range(3.5,20)
	$ter_mezo.scale.x = nov_longeco
	$ter_fino.position.x = nov_longeco * magia_numero
	$fino_de_la_peco.position.x = (nov_longeco * magia_numero) + truo
	
func preni_finon():
	var fino
	fino = to_global($fino_de_la_peco.position)
	
	return fino