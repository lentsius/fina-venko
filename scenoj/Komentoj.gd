extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

#	montras la nomitan objekton kiu devas esti de tipo Label. Trovas laux nomo kiu estas pasita.
func montri(kiun : String = "Fek"):
	var teksto : Label = get_node(kiun)
	$Tvino.interpolate_property(teksto, 'rect_scale', Vector2(0.1,0.1), Vector2(1,1), 1, Tween.TRANS_BOUNCE, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property(teksto, 'modulate', Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.start()
	yield($Tvino, 'tween_completed')
	$Tvino.interpolate_property(teksto, 'rect_scale', Vector2(1,1), Vector2(2,2), 1, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property(teksto, 'modulate', Color(1,1,1,1), Color(1,1,1,0), 0.8, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)
	$Tvino.interpolate_property(teksto, 'rect_rotation', 0, 720, 0.8, Tween.TRANS_CUBIC, Tween.EASE_OUT, 0)	
	$Tvino.start()
	yield($Tvino, 'tween_completed')

func _on_Esperantisto_sendi_staton(numero):
	if numero == 1:
		montri("Fek")
